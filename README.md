# My portfolio and CV
This site is used to describe me and my work.

All technologies and trademarks are properties of their owners. The text/information on the site is my property, Copyright © 2012-2020 Daniel Holm. All rights reserved.

The HTML-structure and CSS file are released under the GNU GPL v3 license.